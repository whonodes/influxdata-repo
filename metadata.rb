name 'influxdata-repo'
maintainer 'Matthew Iverson'
maintainer_email 'matthewdiverson@gmail.com'
license 'MIT'
description 'Installs/Configures influxdata-repo'
version '0.1.0'
chef_version '>= 13.0'

%w(debian ubuntu centos redhat).each do |os|
  supports os
end

issues_url 'https://bitbucket.org/whonodes/influxdata-repo/issues'
source_url 'https://bitbucket.org/whonodes/influxdata-repo'
